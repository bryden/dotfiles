#####################################
# Install some general dependencies #
#####################################
sudo apt-get update
sudo apt-get upgrade
sudo apt install cmake vim zsh tmux powerline ack composer docker wget chromium-browser keepass2 nitrogen arandr compton htop neofetch ctags
sudo apt-get autoremove

##############
# LAMP Stack #
##############
sudo apt install apache2
sudo apt install mysql-server
sudo apt install php-7.2 php-curl php-mysql php-dompdf php-xdebug php-dev php-mbstring
sudo updatedb # need to run this before running locate
locate xdebug.so # this get the xdebug.so location
ln -s ~/dotfiles/20-xdebug.ini /etc/php/7.2/apache2/conf.d/20-xdebug.ini

#################
# Install fonts #
#################
wget https://github.com/source-foundry/Hack/releases/download/v3.003/Hack-v3.003-ttf.zip -P ~/.fonts
wget https://github.com/powerline/fonts/blob/master/DroidSansMonoSlashed/Droid%20Sans%20Mono%20Slashed%20for%20Powerline.ttf -P ~/.fonts
unzip ~/.fonts/Hack-v3.003-ttf.zip -d ~/.fonts

########################
# Instal fzf fuzzyfind #
########################
git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
~/.fzf/install

################
# Install Rofi #
################
sudo apt install rofi
ln -s ~/dotfiles/config/rofi ~/.config/rofi

###################
# Install Polybar #
###################
sudo apt install cairo xcb-proto 
git clone --branch 3.2 --recursive https://github.com/jaagr/polybar ~/bin/polybar
mkdir ~/bin/polybar/build && cd ~/bin/polybar/build
cmake ..
sudo make install
ln -s ~/dotfiles/config/polybar ~/.config/polybar

#####################
# Install oh-my-zsh #
#####################
sh -c "$(wget https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O -)"
chsh -s $(which zsh)

if [ -f ~/.zshrc ]; then
  mv ~/.zshrc ~/.zshrc_before_script
fi

ln -s ~/dotfiles/.zshrc ~/.zshrc

###############################
# Install tmux plugin manager #
###############################
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
ln -s ~/dotfiles/.tmux.conf ~/.tmux.conf
tmux source ~/.tmux.conf

##################
# Install vundle #
##################
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
ln -s ~/dotfiles/.vimrc ~/.vimrc
vim +PluginInstall +qall

# Create some web-app bins
# bitcoin
# email
# slack
